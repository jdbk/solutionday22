#/bin/bash
vesctl configuration create app_type -i ../kad/ves-objects/apptype.yml
vesctl configuration create app_type -i ../argocd/ves-objects/apptype.yml
vesctl configuration create app_type -i ../prometheus/ves-objects/apptype.yml

vesctl configuration create app_setting -i ../argocd/ves-objects/appsettings.yml
vesctl configuration create app_setting -i ../kad/ves-objects/appsettings.yml
vesctl configuration create app_setting -i ../prometheus/ves-objects/appsettings.yml

