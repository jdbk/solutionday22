#/bin/bash
vesctl configuration apply http_loadbalancer -i ../prometheus/ves-objects/prometheus-public.yaml
vesctl configuration apply http_loadbalancer -i ../kad/ves-objects/kad-public.yaml
vesctl configuration apply http_loadbalancer -i ../argocd/ves-objects.yml/argo-public.yaml
